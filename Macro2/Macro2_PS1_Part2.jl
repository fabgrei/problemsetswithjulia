## Solution to the second part of PS1 (RBC)
## by Fabian Greimel

## This code uses a Julia version of the gensys methods, which is part of the DSGE.jl package (provided by the Fed NY). The steady state is computed numerically using an implemented rootfinding function. The matrices for the gensys method are computed without any manual derivatives. Instead I use Automatic Differentiation. This is a very fast method to compute EXACT derivatives.

## Calculating steady states and derivatives by hand is error-prone. So I think one should better leave it to computers.

α = 0.66
β = 0.96
δ = 0.04
ρ = 0.9
θ_bar = 0.2
μ = 3.

## Define production function, utility functions and derivatives
F(K, L, θ) = exp(θ) * K^(1 - α) * L^α

using ForwardDiff
∇F = ForwardDiff.gradient(x -> F(x[1], x[2], x[3]))
∇F([1, 1, 1.])

F_K(K, L, θ) = ∇F([K, L, θ])[1]
F_L(K, L, θ) = ∇F([K, L, θ])[2]
F_θ(K, L, θ) = ∇F([K, L, θ])[3]

U(c, n) = log(c) + μ * (1 - n)
∇U = ForwardDiff.gradient(x -> U(x[1], x[2]))
U_c(c, n) = ∇U([c, n])[1]
U_n(c, n) = ∇U([c, n])[2]

## The first order conditions are given by
function V_1(c, n, c_next, n_next, k_next, θ_next)
    U_c(c, n) - β * U_c( c_next, n_next) * ( (1 - δ) + F_K(k_next, n_next, θ_next) )
end

function V_2(c, n, k, θ)
    U_c( c, n) * F_L(k, n, θ) + U_n( c, n)
end

## Compute the steady state
function get_SteadyState(n_star)
    k = fzeros(k -> V_1(1, n_star, 1, n_star, k, θ_bar), [0.00001, 99])
    k = k[1]
    c = (1- δ) * k + F(k, n_star, θ_bar) - k
    (k, c)
end

function compute_error(n)
    k, c = get_SteadyState(n)
    V_2(c, n, k, θ_bar)
end

using Roots
nstar = fzero(compute_error,[0.0001, 0.99])

kstar, cstar = get_SteadyState(nstar)

## Put the first order conditions into one function
function V(k_nnext, k_next, k, n_next, n, θ_next, θ)
    c_next = (1- δ) * k_next + F(k_next, n_next, θ_next) - k_nnext
    c = (1- δ) * k + F(k, n, θ) - k_next
    [V_1(c, n, c_next, n_next, k_next, θ_next), V_2(c, n, k, θ)]
end

## Lets check if the F.O.C.s are satisfied by the steady state
V(kstar, kstar, kstar, nstar, nstar, θ_bar, θ_bar)
## yes they are

## Compute the derivatives at the steady state values
using ForwardDiff
JV = jacobian(x -> V(x[1], x[2], x[3], x[4], x[5], x[6], x[7]))
JacobStSt = JV([kstar, kstar, kstar, nstar, nstar, θ_bar, θ_bar]) 

####################################
### Constructing the matrices ######
####################################

## Calculating Γ0
## we need the missing block

## y_t = [k_next, n, θ, E_k_nnext, n_next, E_θ_next]
ordering_y_t = [2, 5, 7, 1, 4, 6]
Block1 = JacobStSt[:, ordering_y_t]

Γ0 = [Block1;
      eye(3) zeros(3, 3);
      0 0 1 0 0 0];

## For the second block we only need the derivatives w.r.t k
Block2 = JacobStSt[:, 3]
Γ1 = [-Block2 zeros(2,5);
      zeros(3, 3) eye(3);
      0 0 ρ 0 0 0]

Ψ = [zeros(5);
     1]

Π = [zeros(2, 3);
     eye(3);
     zeros(1, 3)]

########################################
### Solving the model with gensys ######
########################################

## the gensys method has been implemented for Julia by the Fed NY
## they provide a package DSGE
using DSGE
G1, C, impact, fmat, fwt, ywt, gev, eu, loose = gensys(Γ0, Γ1, zeros(6), Ψ, Π, 1);

## Plotting the impulse responses

T = 40
ϵ = 0.03                                # productivity shock

N = size(impact)[1]

IRF = zeros(N+1, T)
IRF[1:N, 1] = impact * ϵ

## compute the first consumption
knext = IRF[1, 1] ;
k = 0;
n = IRF[2, 1] ;
θ = IRF[3, 1] ;
IRF[N+1,1] = (1 - δ) * k + F(k + kstar, n + nstar, θ + θ_bar) - F(kstar, nstar, θ_bar) - knext;

for t = 2:T
    IRF[1:N, t] = G1* IRF[1:N, t-1]
    knext = IRF[1, t] 
    k = IRF[1, t-1] 
    n = IRF[2, t] 
    θ = IRF[3, t] 
    IRF[N+1,t] = (1 - δ) * k + F(k + kstar, n + nstar, θ + θ_bar) - F(kstar, nstar, θ_bar) - knext
end

using PyPlot
## capital
figure(1)
subplot(1,3,1)
plot(1:T, IRF[1,:][:])
xlabel("time")
ylabel("deviation from steadystate")
title("capital")
subplot(1,3,2)
plot(1:T, IRF[2,:][:])
xlabel("time")
title("hours")
subplot(1,3,3)
plot(1:T, IRF[N+1,:][:])
xlabel("time")
title("consumption")
